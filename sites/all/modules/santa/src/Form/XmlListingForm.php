<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\santa\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ChangedCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Contribute form.
 */
class XmlListingForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xml_listing_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL) {
		$extensions = array('application/xml' => 'XML','text/csv' => 'CSV');
		
		$form['#suffix'] = '<div id="xml-table-content"></div>';
    
		$form['#attributes'] = [
			'class' => ['xml-listing-form-container'],
		];
		
		$current_user = \Drupal::currentUser();
		$userRole = $current_user->getRoles();
		
		$showOperation = 0;
		if(in_array('super_admin', $userRole) || in_array('administrator', $userRole)) {
			$showOperation = 1;
		}
		
		if($showOperation)
			$header = array(t('Resource'), t('Type'), t('Download'), t('Preview'), t('Operations'));
		else
			$header = array(t('Resource'), t('Type'), t('Download'), t('Preview'));
		
    $form['xml'] = array(
			'#type' => 'table',
			'#header' => $header,
		);
		
		$records = $this->getXmlFiles($nid);
		$records->allowRowCount = TRUE;
		$count = $records->rowCount();
		
		if($count == 0) {
			return [];
		}
		
		$xml_node = node_load($nid);
		$xml_title = $xml_node->getTitle();
		
		foreach ($records as $record) {
			$timeAgo = t('(created during the last @time)', array('@time' => \Drupal::service('date.formatter')->formatTimeDiffSince($record->created_date, ['granularity' => 1])));
			// Resources
			$form['xml'][$record->id]['resource'] = array(
				'#plain_text' => 'Listing of ' . $xml_title . ' ' . $timeAgo,
			);
			
			// Type
			$form['xml'][$record->id]['type'] = array(
				'#plain_text' => isset($extensions[$record->type]) ? $extensions[$record->type] : $record->type,
			);
			
			// Download
			$form['xml'][$record->id]['download'] = array(
				'#type' => 'link',
				'#title' => $this->t('Download'),
				'#url' => Url::fromRoute('file_download.link', array('scheme' => 'public', 'fid' => $record->file_id)),
			);
			
			// Preview
			$form['xml'][$record->id]['preview'] = array(
				'#type' => 'radio',
				'#parents' => ['xml'],
				'#return_value' => $record->file_id,
				'#attributes' => ['xml_title' => 'Listing of ' . $xml_title],
				'#ajax' => [
					'callback' => '::xmlPreview',
					'wrapper' => 'xml-table-content',
					'progress' => [
						'type' => 'throbber',
						'message' => t('Loading Record...'),
					],
				],
			);
			
			if($showOperation) {
				$form['xml'][$record->id]['operations'] = array(
					'#type' => 'submit',
					'#value' => 'Delete',
					'#name' => 'delete_' . $record->id,
					'#id' => $record->id,
					'#submit' => ['::xmlDelete'],
				);
				
			}
			
		}
		
		return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
	
  /**
   * {@inheritdoc}
   */
  public function xmlPreview(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
		
		$tableContent = '';
		if(isset($element['#value']) && !empty($element['#value'])) {
			$tableContent = $this->loadXmlRecordsTable($element['#value']);
		}
		
		$output = '<div id="xml-table-content"><h4>'. $element['#attributes']['xml_title'] .'</h4><i>This is preview. If you would like to view the full resource, please download it above.</i>' . drupal_render($tableContent) . '</div>';
		
		$ajax_response = new AjaxResponse();
		$ajax_response->addCommand(new HtmlCommand('#xml-table-content', $output));
	
		return $ajax_response;
  }
	
	/**
	 *
	*/
	public function loadXmlRecordsTable($fid) {
		$file = File::load($fid);
		$type = $file->getMimeType();
		$cid = 'xml_csv_data_' . $fid;
		
		if ($cache = \Drupal::cache()->get($cid)) {
			$data = $cache->data;
		}
		else {
			$filePath = file_create_url($file->getFileUri());
			$header = $rows = [];
			$item = 0;
			switch($type) {
				case 'application/xml':
					$xml = simplexml_load_file($filePath);
					foreach($xml->children() as $record) {
						$record = (array)$record;
						$header = array_keys($record);
						$rows[] = $record;
					}
					break;
				case 'text/csv':
					$delimiter = ',';
					if (($handle = fopen($filePath, 'r')) !== FALSE) {
						while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
							if(!$header)
								$header = $row;
							else
								$rows[] = array_combine($header, $row);
						}
						fclose($handle);
					}
					break;
			}
			
			$data = ['header' => $header, 'rows' => $rows];
			\Drupal::cache()->set($cid, $data);
		}
		
		$form = [
			'#type' => 'table',
			'#header' => isset($data['header']) ? $data['header'] : [],
			'#footer' => [
        [
          'data' => isset($data['header']) ? $data['header'] : [],
        ]
      ],
			'#rows' => isset($data['rows']) ? $data['rows'] : [],
      '#attributes' => [
        'id' => 'xml-preview-datatables', 
      ]
		];
		
		return $form;
	} 
	
	/**
	* {@inheritdoc}
	*/
	public function xmlDelete(array &$form, FormStateInterface $form_state) {
		$element = $form_state->getTriggeringElement();
		$id = $element['#id'];
		$this->deleteXmlFiles($id);
		
		drupal_flush_all_caches();
		$form_state->setRebuild(FALSE);
	}
	
  /**
  * Get XML Files
  */
  public function getXmlFiles($nid = '') {
    $connection = Database::getConnection();
    $query = $connection->select('santa_xml_upload', 'xml');
    $query->condition('xml.nid', $nid);
    $query->fields('xml');
    $results = $query->execute();
    return $results;
    
  }
  
  /**
  * Delete XML Files
  */
  public function deleteXmlFiles($id) {
    $connection = Database::getConnection();
    $query = $connection->delete('santa_xml_upload');
    $query->condition('id', $id);
    $results = $query->execute();
  }


}
