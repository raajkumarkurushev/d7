<?php


/**
 * Filter Field
 */
function filter_fields_set(&$form, &$form_state, $node) {
	$nid = $node->nid;
	
	$form['filter_fields'] = [
		'#type' => 'fieldset', 
		'#title' => t('Filter'), 
		'#weight' => 5, 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
		'#limit_validation_errors' => true,
	];
		
	if (empty($form_state['report_filer_count'])) {
		$form_state['report_filer_count'] = 1;
  }
	
	$form['filter_fields']['filter_fields_field'] = [
		'#type' => 'container',
		'#weight' => 2,
		'#attributes' => ['class' => ['field-filter-set']]
	];
	
	$form['filter_fields']['filter_fields_field']['name'] = [
		'#type' => 'select', 
		'#title' => t('Select Component'),
		//'#required' => TRUE, 
		'#options' => get_report_column_components($nid),
	];
	
	$form['filter_fields']['filter_fields_field']['operation'] = [
		'#type' => 'select', 
		'#title' => t('Operation'),
		//'#required' => TRUE, 
		'#options' => ['==' => 'Equal to', '<>' => 'Not Equal to', 'like' => 'Contain',],
	];
	
	$form['filter_fields']['filter_fields_field']['value'] = [
		'#type' => 'textfield', 
		'#title' => t('Value'),
		//'#required' => TRUE, 
	];
		
	$form['filter_fields']['filter_fields_field_set'] = [
		'#type' => 'container',
		'#weight' => 1,
		'#attributes' => ['id' => ['filter-fields-container'], 'class' => ['field-filter-set']]
	];
	
	if (empty($form_state['report_filer_count'])) {
		$form_state['report_filer_count'] = 0;
  }
	
	for($i=1;$i<$form_state['report_filer_count'];$i++) {
		$field_set = $form['filter_fields']['filter_fields_field'];
		
		$form['filter_fields']['filter_fields_field_set'][$i] = [
			'#type' => 'container',
			'#attributes' => ['id' => ['filter-fields-container-'.$i], 'class' => ['field-filter-set'.$i]]
		];
			
		foreach(element_children($field_set) as $field_name) {
			$form['filter_fields']['filter_fields_field_set'][$i][$field_name] = $field_set[$field_name];
			$form['filter_fields']['filter_fields_field_set'][$i][$field_name]['#value'] = isset($form_state['values']['filter_fields']['filter_fields_field_set'][$i][$field_name]) ? $form_state['values']['filter_fields']['filter_fields_field_set'][$i][$field_name] : $form_state['input']['filter_fields']['filter_fields_field'][$field_name];
			
			$form['filter_fields']['filter_fields_field_set'][$i][$field_name]['#disabled'] = true;
			
		}
		
		$form['filter_fields']['filter_fields_field_set'][$i]['remove'] = [
			'#type' => 'submit', 
			'#value' => t('Remove' . $i),
			'#field_set_position' => $i,
			'#submit' => array(
				'ajax_delete_filter_fields',
			),
			'#ajax' => array(
				// 'method' => 'before',
				'callback' => 'ajax_remove_filter_fields',
				'wrapper' => 'filter-fields-container-'.$i,
			),
		];
	
	}
	
	$form['filter_fields']['add_filter'] = array(
		'#type' => 'submit',
		'#value' => t('Add Filter'),
		'#weight' => 3,
		'#submit' => array(
      'ajax_add_filter_fields',
    ),
		'#ajax' => array(
			'callback' => 'ajax_add_filter_fields_callback',
			'wrapper' => 'filter-fields-container',
		),
	);
	
}	

function ajax_add_filter_fields(&$form, &$form_state){
	$form_state['report_filer_count']++;
  $form_state['rebuild'] = TRUE;
}

function ajax_add_filter_fields_callback(&$form, &$form_state){
	return $form['filter_fields']['filter_fields_field_set'];
}

function ajax_delete_filter_fields(&$form, &$form_state){
	$form_state['report_filer_count']--;
  $form_state['rebuild'] = TRUE;
}

function ajax_remove_filter_fields(&$form, &$form_state){
	return [];
}

