<?php


/**
 * sort Field
 */
function field_sort_set(&$form, &$form_state, $node) {
	$nid = $node->nid;
	
	$form['field_sort'] = [
		'#type' => 'fieldset', 
		'#title' => t('Sort By'), 
		'#weight' => 5, 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
		'#limit_validation_errors' => array(),
	];
		
	if (empty($form_state['field_sort_count'])) {
		$form_state['field_sort_count'] = 1;
  }
	
	$form['field_sort']['field_sort_field'] = [
		'#type' => 'container',
		'#weight' => 2,
		'#attributes' => ['class' => ['field-sort-set']]
	];
	
	$form['field_sort']['field_sort_field']['name'] = [
		'#type' => 'select', 
		//'#required' => TRUE, 
		'#title' => t('Select Component'),
		'#options' => get_report_column_components($nid),
	];
	
	$form['field_sort']['field_sort_field']['type'] = [
		'#type' => 'select', 
		'#title' => t('Sorting'),
		//'#required' => TRUE,
		'#options' => ['asc' => 'Asc', 'desc' => 'Desc'],
	];
		
	$form['field_sort']['field_sort_field_set'] = [
		'#type' => 'container',
		'#weight' => 1,
		'#attributes' => ['id' => ['field-sort-container'], 'class' => ['field-sort-set']]
	];
	
	if (empty($form_state['field_sort_count'])) {
		$form_state['field_sort_count'] = 0;
  }
	
	for($i=1;$i<$form_state['field_sort_count'];$i++) {
		$field_set = $form['field_sort']['field_sort_field'];
		
		$form['field_sort']['field_sort_field_set'][$i] = [
			'#type' => 'container',
			'#attributes' => ['id' => ['field-sort-container-'.$i], 'class' => ['field-sort-set'.$i]]
		];
			
		foreach(element_children($field_set) as $field_name) {
			$form['field_sort']['field_sort_field_set'][$i][$field_name] = $field_set[$field_name];
			$form['field_sort']['field_sort_field_set'][$i][$field_name]['#value'] = isset($form_state['values']['field_sort']['field_sort_field_set'][$i][$field_name]) ? $form_state['values']['field_sort']['field_sort_field_set'][$i][$field_name] : $form_state['input']['field_sort']['field_sort_field'][$field_name];
			
			$form['field_sort']['field_sort_field_set'][$i][$field_name]['#disabled'] = true;
			
		}
		
		$form['field_sort']['field_sort_field_set'][$i]['remove'] = [
			'#type' => 'submit', 
			'#value' => t('Remove' . $i),
			'#field_set_position' => $i,
			'#submit' => array(
				'ajax_delete_field_sort',
			),
			'#ajax' => array(
				// 'method' => 'before',
				'callback' => 'ajax_remove_field_sort',
				'wrapper' => 'field-sort-container-'.$i,
			),
		];
	
	}
	
	$form['field_sort']['add_sort'] = array(
		'#type' => 'submit',
		'#value' => t('Add sort'),
		'#weight' => 3,
		'#submit' => array(
      'ajax_add_field_sort',
    ),
		'#ajax' => array(
			'callback' => 'ajax_add_field_sort_callback',
			'wrapper' => 'field-sort-container',
		),
	);
	
}	

function ajax_add_field_sort(&$form, &$form_state){
	$form_state['field_sort_count']++;
  $form_state['rebuild'] = TRUE;
}

function ajax_add_field_sort_callback(&$form, &$form_state){
	return $form['field_sort']['field_sort_field_set'];
}

function ajax_delete_field_sort(&$form, &$form_state){
	$form_state['field_sort_count']--;
  $form_state['rebuild'] = TRUE;
}

function ajax_remove_field_sort(&$form, &$form_state){
	return [];
}

