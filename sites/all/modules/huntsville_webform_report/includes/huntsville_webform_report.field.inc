<?php


/**
 *  Field Column
 */
function field_column_set(&$form, &$form_state, $node) {
	$nid = $node->nid;
	
	$form['field_column'] = [
		'#type' => 'fieldset', 
		'#title' => t('Report Columns'), 
		'#weight' => 5, 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
		'#limit_validation_errors' => array(),
	];
	
	$form['field_column']['field_column_field'] = [
		'#type' => 'container',
		'#weight' => 2,
		'#attributes' => ['class' => ['field-column-set']]
	];
	
	$form['field_column']['field_column_field']['name'] = [
		'#type' => 'select', 
		'#title' => t('Select Component'),
		'#options' => get_report_column_components($nid),
		//'#required' => TRUE, 
	];
	
	$form['field_column']['field_column_field_set'] = [
		'#type' => 'container',
		'#weight' => 1,
		'#attributes' => ['id' => ['field-column-container'], 'class' => ['field-column-set']]
	];
	
	if (empty($form_state['report_field_count'])) {
		$form_state['report_field_count'] = [];
  }
	
	foreach($form_state['report_field_count'] as $i => $j) {
		$field_set = $form['field_column']['field_column_field'];
		
		$form['field_column']['field_column_field_set'][$i] = [
			'#type' => 'container',
			'#attributes' => ['id' => ['field-column-container-'.$i], 'class' => ['field-column-set'.$i]]
		];
			
		foreach(element_children($field_set) as $field_name) {
			$form['field_column']['field_column_field_set'][$i][$field_name] = $field_set[$field_name];
			$form['field_column']['field_column_field_set'][$i][$field_name]['#value'] = isset($form_state['values']['field_column']['field_column_field_set'][$i][$field_name]) ? $form_state['values']['field_column']['field_column_field_set'][$i][$field_name] : $form_state['input']['field_column']['field_column_field'][$field_name];
			
			$form['field_column']['field_column_field_set'][$i][$field_name]['#disabled'] = true;
			
		}
		
		$form['field_column']['field_column_field_set'][$i]['remove'] = [
			'#type' => 'submit', 
			'#value' => t('Remove' . $i),
			'#field_set_position' => $i,
			'#submit' => array(
				'ajax_delete_field_column',
			),
			'#ajax' => array(
				// 'method' => 'before',
				'callback' => 'ajax_remove_field_column',
				'wrapper' => 'field-column-container-'.$i,
			),
		];
	
	}
	
	$form['field_column']['add_Field'] = array(
		'#type' => 'submit',
		'#value' => t('Add Field'),
		'#weight' => 3,
		'#submit' => array(
      'ajax_add_field_column',
    ),
		'#ajax' => array(
			'callback' => 'ajax_add_field_column_callback',
			'wrapper' => 'field-column-container',
		),
	);
	
}	

function ajax_add_field_column(&$form, &$form_state){
	$current_set = end($form_state['report_field_count']);
	$current_set = (int) $current_set;
	$current_set++;
	$form_state['report_field_count'][$current_set] = $current_set;
  $form_state['rebuild'] = TRUE;
}

function ajax_add_field_column_callback(&$form, &$form_state){
	return $form['field_column']['field_column_field_set'];
}

function ajax_delete_field_column(&$form, &$form_state){
	$clicked_button = $form_state['clicked_button'];
	$field_set_position = $clicked_button['#field_set_position'];
	unset($form_state['report_field_count'][$field_set_position]);
	$form_state['rebuild'] = TRUE;
}

function ajax_remove_field_column(&$form, &$form_state){
	return [];
}
