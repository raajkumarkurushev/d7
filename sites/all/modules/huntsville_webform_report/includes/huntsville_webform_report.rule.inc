<?php


/**
 * sort Field
 */
function field_sort_rule(&$form, &$form_state, $node) {
	$nid = $node->nid;
	
	$form['cron'] = [
		'#type' => 'fieldset', 
		'#title' => t('Send Rules'), 
		'#weight' => 6, 
		'#collapsible' => TRUE, 
		'#cllapsed' => FALSE,
	];
	
	$form['cron']['start_date'] = [
		'#type' => 'date_popup',
		'#date_format' => 'm/d/Y h:i',
		'#required' => true,
		'#default_value' => isset($form_state['input']['cron']['start_date']) ? $form_state['input']['cron']['start_date'] : (isset($form_state['values']['cron']['start_date']) ? $form_state['values']['cron']['start_date'] : ''),
	];
	
	
	$form['cron']['send_schedule'] = [
		'#type' => 'checkbox', 
		'#title' => t('Send on Schedule'), 
		'#options' => [1 => 1],
		'#default_value' => isset($form_state['input']['cron']['send_schedule']) ? $form_state['input']['cron']['send_schedule'] : (isset($form_state['values']['cron']['send_schedule']) ? $form_state['values']['cron']['send_schedule'] : ''),
	];
	
	$form['cron']['interval_type'] = [
		'#type' => 'select', 
		'#title' => t('Interval'),
		'#required' => true,
		'#options' => ['month' => 'Monthly', 'day' => 'Daily'],
		'#default_value' => isset($form_state['input']['cron']['interval_type']) ? $form_state['input']['cron']['interval_type'] : (isset($form_state['values']['cron']['interval_type']) ? $form_state['values']['cron']['interval_type'] : ''),
	];
	
	$form['cron']['interval_repeat'] = [
		'#type' => 'textfield', 
		'#title' => t('Repeats Every'),
		'#field_suffix' => t('month(s)'),
		'#size' => 3,
		'#required' => true,
		'#attributes' => array(
			' type' => 'number',
			'min' => 1,
    ),
		'#default_value' => isset($form_state['input']['cron']['interval_repeat']) ? $form_state['input']['cron']['interval_repeat'] : (isset($form_state['values']['cron']['interval_repeat']) ? $form_state['values']['cron']['interval_repeat'] : ''),
	];
	
	$form['cron']['stop_type'] = [
		'#type' => 'radios', 
		'#title' => t('Stop'),
		'#name' => 'stop',
		'#required' => true,
		'#options' => ['on' => 'On', 'after' => 'after'],
		'#default_value' => isset($form_state['input']['cron']['stop_type']) ? $form_state['input']['cron']['stop_type'] : (isset($form_state['values']['cron']['stop_type']) ? $form_state['values']['cron']['stop_type'] : ''),
	];
	
	$form['cron']['end_date'] = [
		'#type' => 'date_popup',
		'#title' => 'End',
		'#title_display' => 'invisible',
		'#date_format' => 'm/d/Y',
		'#states' => [
			'visible' => [
				':input[name="cron[stop_type]"]' => array('value' => 'on'),
			],
			'required' => [
				':input[name="cron[stop_type]"]' => array('value' => 'on'),
			],
		],
		'#default_value' => isset($form_state['input']['cron']['end_date']) ? $form_state['input']['cron']['end_date'] : (isset($form_state['values']['cron']['end_date']) ? $form_state['values']['cron']['end_date'] : ''),
	];
	
	$form['cron']['interval_count'] = [
		'#type' => 'textfield', 
		'#field_suffix' => t('Intervals'),
		'#size' => 3,
		'#attributes' => array(
			' type' => 'number',
			'min' => 1,
    ),
		'#states' => [
			'visible' => [
				':input[name="cron[stop_type]"]' => array('value' => 'after'),
			],
			'required' => [
				':input[name="cron[stop_type]"]' => array('value' => 'after'),
			],
		],
		'#default_value' => isset($form_state['input']['cron']['interval_count']) ? $form_state['input']['cron']['interval_count'] : (isset($form_state['values']['cron']['interval_count']) ? $form_state['values']['cron']['interval_count'] : ''),
	];
	
}