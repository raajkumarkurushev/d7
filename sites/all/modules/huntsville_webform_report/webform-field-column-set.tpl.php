<table>
	<thead>
		<tr>
			<th>Component</th>
			<th>Operation</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach(element_children($element) as $key): ?>
		<tr>
			<td><?php print drupal_render($element[$key]['name']); ?></td>
			<td><?php print drupal_render($element[$key]['remove']); ?></td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<div id="report-columns-container">
			</div>
		</tr>
	</tbody>
</table>